Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2019-2022, Joe Drago.
License: Apache-2.0 or BSD-2-clause

Files: android_jni/avifandroidjni/src/androidTest/assets/avif/* tests/data/io/* tests/data/sources/* third_party/*
Copyright:
 2019-2022 Joe Drago <joedrago@gmail.com>
 2023-2025 Google LLC
License: BSD-2-Clause

Files: CMakeLists.txt
 README.md
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: android_jni/avifandroidjni/src/main/*
Copyright: 2020, 2022-2025, Google LLC
License: BSD-2-clause

Files: android_jni/gradlew
 android_jni/gradlew.bat
Copyright: 2015, the original author or authors.
License: Apache-2.0

Files: apps/*
Copyright: 2020, 2022-2025, Google LLC
License: BSD-2-clause

Files: apps/avifdec.c
 apps/avifenc.c
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: apps/shared/*
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: apps/shared/avifexif.c
 apps/shared/avifexif.h
Copyright: 2020, 2022-2025, Google LLC
License: BSD-2-clause

Files: apps/shared/iccmaker.c
 apps/shared/iccmaker.h
Copyright: 2022, 2023, Yuan Tong.
License: BSD-2-clause

Files: contrib/CMakeLists.txt
 contrib/irefmerge.coffee
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: contrib/gdk-pixbuf/CMakeLists.txt
 contrib/gdk-pixbuf/loader.c
Copyright: 2020, Emmanuel Gil Peyrot.
License: BSD-2-clause

Files: debian/*
Copyright: 2020, Boyuan Yang <byang@debian.org>
License: CC0-1.0

Files: examples/*
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: ext/libxml2.cmd
Copyright: no-info-found
License: Expat

Files: include/*
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: include/avif/avif_cxx.h
Copyright: 2020, 2022-2025, Google LLC
License: BSD-2-clause

Files: libargparse/*
Copyright: 2017, K. Murray
License: Expat

Files: src/*
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: src/codec_avm.c
 src/codec_libgav1.c
 src/colrconvert.c
 src/compliance.cc
 src/exif.c
 src/gainmap.c
 src/reformat_libsharpyuv.c
 src/sampletransform.c
Copyright: 2020, 2022-2025, Google LLC
License: BSD-2-clause

Files: src/codec_svt.c
Copyright: 2020, Cloudinary.
License: BSD-2-clause

Files: src/obu.c
Copyright: 2018, VideoLAN and dav1d authors
 2018, Two Orioles, LLC
License: BSD-2-clause

Files: src/properties.c
Copyright: 2024, Brad Hards.
License: BSD-2-clause

Files: tests/*
Copyright: 2020, 2022-2025, Google LLC
License: BSD-2-clause

Files: tests/CMakeLists.txt
 tests/aviftest.c
 tests/avifyuv.c
Copyright: 2019-2022, Joe Drago.
License: BSD-2-clause

Files: tests/cmd_test_common.sh
 tests/test_all_configurations.sh
 tests/test_cmd.sh
 tests/test_cmd_animation.sh
 tests/test_cmd_avm.sh
 tests/test_cmd_avm_lossless.sh
 tests/test_cmd_gainmap.sh
 tests/test_cmd_grid.sh
 tests/test_cmd_icc_profile.sh
 tests/test_cmd_lossless.sh
 tests/test_cmd_metadata.sh
 tests/test_cmd_progressive.sh
 tests/test_cmd_stdin.sh
 tests/test_cmd_targetsize.sh
Copyright: 2022-2025, Google LLC
License: Apache-2.0

Files: tests/data/*
Copyright: no-info-found
License: CC-BY or CC-BY-NC-2.5

Files: tests/gtest/avifchangesettingtest.cc
 tests/gtest/avifprogressivetest.cc
Copyright: 2022, 2023, Yuan Tong.
License: BSD-2-clause

Files: tests/oss-fuzz/build.sh
Copyright: 2020, Google Inc.
License: Apache-2.0

Files: android_jni/avifandroidjni/src/androidTest/assets/animated_avif/Chimera-AV1-10bit-480x270.avif android_jni/avifandroidjni/src/androidTest/assets/avif/blue-and-magenta-crop.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile0.10bpc.yuv420.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile0.10bpc.yuv420.monochrome.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile0.8bpc.yuv420.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile0.8bpc.yuv420.monochrome.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile1.10bpc.yuv444.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile1.8bpc.yuv444.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile2.10bpc.yuv422.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile2.12bpc.yuv420.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile2.12bpc.yuv420.monochrome.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile2.12bpc.yuv422.avif android_jni/avifandroidjni/src/androidTest/assets/avif/fox.profile2.12bpc.yuv444.avif cmake/Modules/Findaom.cmake cmake/Modules/Finddav1d.cmake cmake/Modules/Findlibyuv.cmake cmake/Modules/Findrav1e.cmake cmake/Modules/Findsvt.cmake tests/data/arc_triomphe_extent1000_nullbyte_extent1310.avif tests/data/clap_irot_imir_non_essential.avif tests/data/clop_irot_imor.avif tests/data/color_grid_alpha_grid_gainmap_nogrid.avif tests/data/color_grid_alpha_grid_tile_shared_in_dimg.avif tests/data/color_grid_alpha_nogrid.avif tests/data/color_grid_gainmap_different_grid.avif tests/data/color_nogrid_alpha_nogrid_gainmap_grid.avif tests/data/colors-animated-12bpc-keyframes-0-2-3.avif tests/data/colors-animated-8bpc-audio.avif tests/data/colors-animated-8bpc.avif tests/data/colors_hdr_p3.avif tests/data/colors_hdr_rec2020.avif tests/data/colors_hdr_srgb.avif tests/data/colors_sdr_srgb.avif tests/data/colors_text_hdr_p3.avif tests/data/colors_text_hdr_rec2020.avif tests/data/colors_text_hdr_srgb.avif tests/data/colors_text_sdr_srgb.avif tests/data/colors_text_wcg_hdr_rec2020.avif tests/data/colors_text_wcg_sdr_rec2020.avif tests/data/colors_wcg_hdr_rec2020.avif tests/data/io/cosmos1650_yuv444_10bpc_p3pq.avif tests/data/io/kodim03_yuv420_8bpc.avif tests/data/kodim03_yuv420_8bpc.y4m tests/data/kodim23_yuv420_8bpc.y4m tests/data/paris_icc_exif_xmp.avif tests/data/seine_hdr_gainmap_small_srgb.avif tests/data/seine_hdr_gainmap_srgb.avif tests/data/seine_hdr_rec2020.avif tests/data/seine_hdr_srgb.avif tests/data/seine_sdr_gainmap_big_srgb.avif tests/data/seine_sdr_gainmap_gammazero.avif tests/data/seine_sdr_gainmap_notmapbrand.avif tests/data/seine_sdr_gainmap_srgb.avif tests/data/sofa_grid1x5_420.avif tests/data/sofa_grid1x5_420_dimg_repeat.avif tests/data/sofa_grid1x5_420_reversed_dimg_order.avif tests/data/sources/seine.psd tests/data/supported_gainmap_writer_version_with_extra_bytes.avif tests/data/unsupported_gainmap_minimum_version.avif tests/data/unsupported_gainmap_version.avif tests/data/unsupported_gainmap_writer_version_with_extra_bytes.avif tests/data/webp_logo_animated.y4m
Copyright:
 2019-2022 Joe Drago <joedrago@gmail.com>
 2023-2025 Google LLC
License: BSD-2-Clause

Files: cmake/Modules/Findaom.cmake cmake/Modules/Finddav1d.cmake cmake/Modules/Findlibyuv.cmake cmake/Modules/Findrav1e.cmake cmake/Modules/Findsvt.cmake
Copyright: 2020 Andreas Schneider <asn@cryptomilk.org>
License: BSD-3-Clause

Files: cmake/Modules/Findlibgav1.cmake cmake/Modules/Findlibsharpyuv.cmake
Copyright: 2020 Google LLC
License: BSD-3-Clause

Files: cmake/Modules/Findlibgav1.cmake cmake/Modules/Findlibsharpyuv.cmake
Copyright: 2022 Google LLC
License: BSD-3-Clause
